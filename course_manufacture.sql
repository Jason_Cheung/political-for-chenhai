/*
 Navicat Premium Data Transfer

 Source Server         : MySQL_test
 Source Server Type    : MySQL
 Source Server Version : 50610
 Source Host           : localhost:3306
 Source Schema         : course_manufacture

 Target Server Type    : MySQL
 Target Server Version : 50610
 File Encoding         : 65001

 Date: 19/01/2022 01:55:36
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assist_info
-- ----------------------------
DROP TABLE IF EXISTS `assist_info`;
CREATE TABLE `assist_info`
(
    `assist_id`       int(11) NOT NULL,
    `assist_phone`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `assist_name`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `assist_passwd`   varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `assist_rg_time`  datetime(0) NOT NULL,
    `assist_mod_time` datetime(0) NOT NULL,
    PRIMARY KEY (`assist_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for course_assist
-- ----------------------------
DROP TABLE IF EXISTS `course_assist`;
CREATE TABLE `course_assist`
(
    `course_assist_id`       int(11) NOT NULL,
    `course_id`              int(11) NOT NULL,
    `assist_id`              int(11) NOT NULL,
    `teacher_id`             int(11) NOT NULL,
    `course_assist_add_time` datetime(0) NOT NULL,
    `course_assist_mod_time` datetime(0) NOT NULL,
    PRIMARY KEY (`course_assist_id`) USING BTREE,
    INDEX                    `course_id`(`course_id`) USING BTREE,
    INDEX                    `assist_id`(`assist_id`) USING BTREE,
    INDEX                    `teacher_id`(`teacher_id`) USING BTREE,
    CONSTRAINT `course_assist_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course_info` (`course_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `course_assist_ibfk_2` FOREIGN KEY (`assist_id`) REFERENCES `assist_info` (`assist_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `course_assist_ibfk_3` FOREIGN KEY (`teacher_id`) REFERENCES `teacher_info` (`teacher_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for course_detail
-- ----------------------------
DROP TABLE IF EXISTS `course_detail`;
CREATE TABLE `course_detail`
(
    `course_detail_id`    int(11) NOT NULL,
    `course_id`           int(11) NOT NULL,
    `course_week`         int(11) NOT NULL,
    `course_date`         date NOT NULL,
    `course_details`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `course_events`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `event_type`          int(255) NOT NULL,
    `event_deadline`      datetime(0) DEFAULT NULL,
    `event_add_time`      datetime(0) NOT NULL,
    `event_mod_time`      datetime(0) NOT NULL,
    `event_last_mod_id`   int(11) NOT NULL,
    `event_last_mod_type` int(11) NOT NULL,
    PRIMARY KEY (`course_detail_id`) USING BTREE,
    INDEX                 `course_id`(`course_id`) USING BTREE,
    CONSTRAINT `course_detail_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course_info` (`course_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for course_document
-- ----------------------------
DROP TABLE IF EXISTS `course_document`;
CREATE TABLE `course_document`
(
    `course_doc_id`     int(11) NOT NULL,
    `course_detail_id`  int(11) NOT NULL,
    `doc_loc`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `doc_state`         int(11) NOT NULL,
    `doc_add_time`      datetime(0) NOT NULL,
    `doc_update_time`   datetime(0) NOT NULL,
    `doc_uploader_id`   int(11) NOT NULL,
    `doc_uploader_type` int(11) NOT NULL,
    PRIMARY KEY (`course_doc_id`) USING BTREE,
    INDEX               `course_detail_id`(`course_detail_id`) USING BTREE,
    CONSTRAINT `course_document_ibfk_1` FOREIGN KEY (`course_detail_id`) REFERENCES `course_detail` (`course_detail_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for course_info
-- ----------------------------
DROP TABLE IF EXISTS `course_info`;
CREATE TABLE `course_info`
(
    `course_id`            int(11) NOT NULL,
    `course_number`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `course_name`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `course_semester`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `course_teacher_id`    int(11) NOT NULL,
    `course_introduce`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `course_add_time`      datetime(0) NOT NULL,
    `course_mod_time`      datetime(0) NOT NULL,
    `course_last_mod_id`   int(255) NOT NULL,
    `course_last_mod_type` int(255) NOT NULL,
    PRIMARY KEY (`course_id`) USING BTREE,
    INDEX                  `course_teacher_id`(`course_teacher_id`) USING BTREE,
    CONSTRAINT `course_info_ibfk_1` FOREIGN KEY (`course_teacher_id`) REFERENCES `teacher_info` (`teacher_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for teacher_info
-- ----------------------------
DROP TABLE IF EXISTS `teacher_info`;
CREATE TABLE `teacher_info`
(
    `teacher_id`       int(11) NOT NULL,
    `teacher_phone`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `teacher_name`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `teacher_passwd`   varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `teacher_rg_time`  datetime(0) NOT NULL,
    `teacher_mod_time` datetime(0) NOT NULL,
    PRIMARY KEY (`teacher_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET
FOREIGN_KEY_CHECKS = 1;
