package com.chenhai.political.mapper;

import com.chenhai.political.entity.Course_info;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Course_info_mapper {
    Course_info getInfo(String number);

    void addCourseInfo(int id, String number, String name, String semester, int tid, String intro);

    void modifyCourseInfo(int id, String number, String name, String semester, String intro, int uid, int type);

    int getLatestID();

    Course_info getCourseNumber(int cid);

    List<Course_info> showAllCourses();

    Course_info showAssignCourse(String number);

    List<Course_info> getTeacherCourseInfo(int tid);
}
