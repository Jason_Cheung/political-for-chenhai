package com.chenhai.political.mapper;

import com.chenhai.political.entity.Teacher_info;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Teacher_info_mapper {

    Teacher_info getInfo(String phone);

    int getLatestID();

    void registerTeacher(int id, String phone, String name, String pwd, String img_loc);

    void modifyTeacher(int id, String phone, String name, String pwd, String img_loc);

    Boolean checkLogin(String phone, String pwd);

    List<Teacher_info> showAllTeachers();

    Teacher_info getInfoByID(int id);
}
