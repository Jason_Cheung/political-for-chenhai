package com.chenhai.political.mapper;

import com.chenhai.political.entity.Course_document;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Course_document_mapper {

    void addDocumentInfo(int docid, int detailid, String loc, int state, int uploaderid, int uploadertype);

    void deleteDocument(String loc);

    int getLatestID();

    List<Course_document> showAssignCourseDocument(int detailid);

    void modifyDocumentPrincipal(int tid, int docid);

    void addDocumentDownloadTimes(int docid);

    Course_document getDocumentInfo(String loc);
}
