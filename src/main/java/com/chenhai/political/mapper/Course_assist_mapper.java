package com.chenhai.political.mapper;

import com.chenhai.political.entity.Course_assist;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Course_assist_mapper {

    List<Course_assist> getInfo(int cid);

    void addCourseAssist(int caid, int cid, int aid, int tid);

    void deleteAssist(int cid, int aid);

    int getLatestID();

    boolean ensureAssistUnique(int cid, int aid);

    List<Course_assist> showAssignCourseAssist(int cid);


}
