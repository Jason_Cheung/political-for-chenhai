package com.chenhai.political.mapper;

import com.chenhai.political.entity.Assist_info;
import org.springframework.stereotype.Repository;

@Repository
public interface Assist_info_mapper {
    Assist_info getInfo(String phone);

    void registerAssist(int id, String phone, String name, String pwd, String img_loc);

    void modifyAssist(int id, String phone, String name, String pwd, String img_loc);

    int getLatestID();

    boolean checkLogin(String phone, String pwd);

    Assist_info showAssignCourseAssistInfo(int aid);

}
