package com.chenhai.political.mapper;

import com.chenhai.political.entity.Course_detail;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface Course_detail_mapper {

    void addDetailInfo(int cdid, int cid, int week, Date date, String details, String events, int type, Timestamp deadline, int uid, int utype);

    Boolean ensureModifyWeekUnique(int cdid, int week);

    Boolean ensureAddWeekUnique(int cid, int week);

    void modifyDetailInfo(int cdid, int week, Date date, String details, String events, int type, Timestamp deadline, int uid, int utype);

    Course_detail getWeekInfo(int cid, int week);

    int getLatestID();

    int getCourseID(int cdid);

    void deleteDetailInfo(int cid, int week);

    List<Course_detail> showCourseAllDetail(int cid);

    void modifyDetailPrincipal(int tid, int cdid);
}
