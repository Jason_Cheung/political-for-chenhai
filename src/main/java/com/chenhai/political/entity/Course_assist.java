package com.chenhai.political.entity;

import java.sql.Timestamp;

public class Course_assist {
    private int course_assist_id;
    private int course_id;
    private int assist_id;
    private int teacher_id;
    private Timestamp course_assist_add_time;
    private Timestamp course_assist_mod_time;

    public int getCourse_assist_id() {
        return course_assist_id;
    }

    public void setCourse_assist_id(int course_assist_id) {
        this.course_assist_id = course_assist_id;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public int getAssist_id() {
        return assist_id;
    }

    public void setAssist_id(int assist_id) {
        this.assist_id = assist_id;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    public Timestamp getCourse_assist_add_time() {
        return course_assist_add_time;
    }

    public void setCourse_assist_add_time(Timestamp course_assist_add_time) {
        this.course_assist_add_time = course_assist_add_time;
    }

    public Timestamp getCourse_assist_mod_time() {
        return course_assist_mod_time;
    }

    public void setCourse_assist_mod_time(Timestamp course_assist_mod_time) {
        this.course_assist_mod_time = course_assist_mod_time;
    }
}
