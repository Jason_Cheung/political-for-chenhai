package com.chenhai.political.entity;

import java.sql.Date;
import java.sql.Timestamp;

public class Course_detail {
    private int course_detail_id;
    private int course_id;
    private int course_week;
    private Date course_date;
    private String course_details;
    private String course_events;
    private int event_type;
    private Timestamp event_deadline;
    private Timestamp event_add_time;
    private Timestamp event_mod_time;
    private int event_last_mod_id;
    private int event_last_mod_type;

    public int getCourse_detail_id() {
        return course_detail_id;
    }

    public void setCourse_detail_id(int course_detail_id) {
        this.course_detail_id = course_detail_id;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public int getCourse_week() {
        return course_week;
    }

    public void setCourse_week(int course_week) {
        this.course_week = course_week;
    }

    public Date getCourse_date() {
        return course_date;
    }

    public void setCourse_date(Date course_date) {
        this.course_date = course_date;
    }

    public String getCourse_details() {
        return course_details;
    }

    public void setCourse_details(String course_details) {
        this.course_details = course_details;
    }

    public String getCourse_events() {
        return course_events;
    }

    public void setCourse_events(String course_events) {
        this.course_events = course_events;
    }

    public int getEvent_type() {
        return event_type;
    }

    public void setEvent_type(int event_type) {
        this.event_type = event_type;
    }

    public Timestamp getEvent_deadline() {
        return event_deadline;
    }

    public void setEvent_deadline(Timestamp event_deadline) {
        this.event_deadline = event_deadline;
    }

    public Timestamp getEvent_add_time() {
        return event_add_time;
    }

    public void setEvent_add_time(Timestamp event_add_time) {
        this.event_add_time = event_add_time;
    }

    public Timestamp getEvent_mod_time() {
        return event_mod_time;
    }

    public void setEvent_mod_time(Timestamp event_mod_time) {
        this.event_mod_time = event_mod_time;
    }

    public int getEvent_last_mod_id() {
        return event_last_mod_id;
    }

    public void setEvent_last_mod_id(int event_last_mod_id) {
        this.event_last_mod_id = event_last_mod_id;
    }

    public int getEvent_last_mod_type() {
        return event_last_mod_type;
    }

    public void setEvent_last_mod_type(int event_last_mod_type) {
        this.event_last_mod_type = event_last_mod_type;
    }
}
