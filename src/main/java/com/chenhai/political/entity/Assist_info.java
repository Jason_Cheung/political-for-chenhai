package com.chenhai.political.entity;

import java.sql.Timestamp;

public class Assist_info {
    private int assist_id;
    private String assist_phone;
    private String assist_name;
    private String assist_passwd;
    private String assist_image;
    private Timestamp assist_rg_time;
    private Timestamp assist_mod_time;

    public int getAssist_id() {
        return assist_id;
    }

    public void setAssist_id(int assist_id) {
        this.assist_id = assist_id;
    }

    public String getAssist_phone() {
        return assist_phone;
    }

    public void setAssist_phone(String assist_phone) {
        this.assist_phone = assist_phone;
    }

    public String getAssist_name() {
        return assist_name;
    }

    public void setAssist_name(String assist_name) {
        this.assist_name = assist_name;
    }

    public String getAssist_passwd() {
        return assist_passwd;
    }

    public void setAssist_password(String assist_passwd) {
        this.assist_passwd = assist_passwd;
    }

    public void setAssist_passwd(String assist_passwd) {
        this.assist_passwd = assist_passwd;
    }

    public String getAssist_image() {
        return assist_image;
    }

    public void setAssist_image(String assist_image) {
        this.assist_image = assist_image;
    }

    public Timestamp getAssist_rg_time() {
        return assist_rg_time;
    }

    public void setAssist_rg_time(Timestamp assist_rg_time) {
        this.assist_rg_time = assist_rg_time;
    }

    public Timestamp getAssist_mod_time() {
        return assist_mod_time;
    }

    public void setAssist_mod_time(Timestamp assist_mod_time) {
        this.assist_mod_time = assist_mod_time;
    }
}
