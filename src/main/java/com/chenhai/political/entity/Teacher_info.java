package com.chenhai.political.entity;

import java.sql.Timestamp;

public class Teacher_info {
    private int teacher_id;
    private String teacher_phone;
    private String teacher_name;
    private String teacher_passwd;
    private String teacher_image;
    private Timestamp teacher_rg_time;
    private Timestamp teacher_mod_time;

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    public String getTeacher_phone() {
        return teacher_phone;
    }

    public void setTeacher_phone(String teacher_phone) {
        this.teacher_phone = teacher_phone;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }

    public String getTeacher_passwd() {
        return teacher_passwd;
    }

    public void setTeacher_passwd(String teacher_passwd) {
        this.teacher_passwd = teacher_passwd;
    }

    public String getTeacher_image() {
        return teacher_image;
    }

    public void setTeacher_image(String teacher_image) {
        this.teacher_image = teacher_image;
    }

    public Timestamp getTeacher_rg_time() {
        return teacher_rg_time;
    }

    public void setTeacher_rg_time(Timestamp teacher_rg_time) {
        this.teacher_rg_time = teacher_rg_time;
    }

    public Timestamp getTeacher_mod_time() {
        return teacher_mod_time;
    }

    public void setTeacher_mod_time(Timestamp teacher_mod_time) {
        this.teacher_mod_time = teacher_mod_time;
    }
}
