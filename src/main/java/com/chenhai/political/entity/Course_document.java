package com.chenhai.political.entity;

import java.sql.Timestamp;

public class Course_document {
    private int course_doc_id;
    private int course_detail_id;
    private String doc_loc;
    private String doc_state;
    private Timestamp doc_add_time;
    private int doc_uploader_id;
    private int doc_uploader_type;
    private int doc_download_times;

    public int getDoc_download_times() {
        return doc_download_times;
    }

    public void setDoc_download_times(int doc_download_times) {
        this.doc_download_times = doc_download_times;
    }

    public int getCourse_doc_id() {
        return course_doc_id;
    }

    public void setCourse_doc_id(int course_doc_id) {
        this.course_doc_id = course_doc_id;
    }

    public int getCourse_detail_id() {
        return course_detail_id;
    }

    public void setCourse_detail_id(int course_detail_id) {
        this.course_detail_id = course_detail_id;
    }

    public String getDoc_loc() {
        return doc_loc;
    }

    public void setDoc_loc(String doc_loc) {
        this.doc_loc = doc_loc;
    }

    public String getDoc_state() {
        return doc_state;
    }

    public void setDoc_state(String doc_state) {
        this.doc_state = doc_state;
    }

    public Timestamp getDoc_add_time() {
        return doc_add_time;
    }

    public void setDoc_add_time(Timestamp doc_add_time) {
        this.doc_add_time = doc_add_time;
    }

    public int getDoc_uploader_id() {
        return doc_uploader_id;
    }

    public void setDoc_uploader_id(int doc_uploader_id) {
        this.doc_uploader_id = doc_uploader_id;
    }

    public int getDoc_uploader_type() {
        return doc_uploader_type;
    }

    public void setDoc_uploader_type(int doc_uploader_type) {
        this.doc_uploader_type = doc_uploader_type;
    }
}
