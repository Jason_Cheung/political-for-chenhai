package com.chenhai.political.entity;

import java.sql.Timestamp;

public class Course_info {
    private int course_id;
    private String course_number;
    private String course_name;
    private String course_semester;
    private int course_teacher_id;
    private String course_introduce;
    private Timestamp course_add_time;
    private Timestamp course_mod_time;
    private int course_last_mod_id;
    private int course_last_mod_type;

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public String getCourse_number() {
        return course_number;
    }

    public void setCourse_number(String course_number) {
        this.course_number = course_number;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCourse_semester() {
        return course_semester;
    }

    public void setCourse_semester(String course_semester) {
        this.course_semester = course_semester;
    }

    public int getCourse_teacher_id() {
        return course_teacher_id;
    }

    public void setCourse_teacher_id(int course_teacher_id) {
        this.course_teacher_id = course_teacher_id;
    }

    public String getCourse_introduce() {
        return course_introduce;
    }

    public void setCourse_introduce(String course_introduce) {
        this.course_introduce = course_introduce;
    }

    public Timestamp getCourse_add_time() {
        return course_add_time;
    }

    public void setCourse_add_time(Timestamp course_add_time) {
        this.course_add_time = course_add_time;
    }

    public Timestamp getCourse_mod_time() {
        return course_mod_time;
    }

    public void setCourse_mod_time(Timestamp course_mod_time) {
        this.course_mod_time = course_mod_time;
    }

    public int getCourse_last_mod_id() {
        return course_last_mod_id;
    }

    public void setCourse_last_mod_id(int course_last_mod_id) {
        this.course_last_mod_id = course_last_mod_id;
    }

    public int getCourse_last_mod_type() {
        return course_last_mod_type;
    }

    public void setCourse_last_mod_type(int course_last_mod_type) {
        this.course_last_mod_type = course_last_mod_type;
    }
}
