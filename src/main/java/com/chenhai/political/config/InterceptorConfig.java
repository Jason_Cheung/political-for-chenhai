package com.chenhai.political.config;

import com.chenhai.political.interceptor.ParamInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;


@Configuration
public class InterceptorConfig extends WebMvcConfigurationSupport {
    @Autowired
    private ParamInterceptor interceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/political/**")
                .excludePathPatterns("/teacher/register")
                .excludePathPatterns("/teacher/login")
                .excludePathPatterns("/assist/register")
                .excludePathPatterns("/assist/login")
                .excludePathPatterns("/coursedoc/download/**");
    }
}
