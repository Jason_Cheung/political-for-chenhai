package com.chenhai.political;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;

@MapperScan("com.chenhai.political.mapper")
@SpringBootApplication
@CrossOrigin
public class PoliticalApplication {

    public static void main(String[] args) {
        SpringApplication.run(PoliticalApplication.class, args);
    }

}
