package com.chenhai.political.interceptor;

import com.chenhai.political.assist.returns.Returns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

@RestController
public class ParamInterceptor implements HandlerInterceptor {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        if(request.getMethod().equals("OPTIONS")) {
            return true;
        }
        String token = request.getHeader("token");
        if (token != null) {
            String id = redisTemplate.opsForValue().get(token);
            if (id != null) {
                redisTemplate.expire(token, 600, TimeUnit.SECONDS); // TOKEN续费
                return true;
            } else {
                returnJson(response);
                return false;
            }
        } else {
            returnJson(response);
            return false;
        }
    }

    private void returnJson(HttpServletResponse response) {
        PrintWriter writer = null;
        Returns r = new Returns();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(200);
        try {
            writer = response.getWriter();
            r.error(10001);
            writer.print(r.formatter(r));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) writer.close();
        }
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object o,
                           ModelAndView view) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object o,
                                Exception e) throws Exception {
    }
}
