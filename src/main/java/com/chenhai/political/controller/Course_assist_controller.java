package com.chenhai.political.controller;

import com.alibaba.fastjson.JSONObject;
import com.chenhai.political.assist.returns.Returns;
import com.chenhai.political.entity.*;
import com.chenhai.political.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/courseassist")
@CrossOrigin
public class Course_assist_controller {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private Assist_service assist_service;

    @Autowired
    private Teacher_service teacher_service;

    @Autowired
    private Course_assist_service course_assist_service;

    @Autowired
    private Course_service course_service;

    @Autowired
    private Course_doc_service course_doc_service;

    @Autowired
    private Course_detail_service course_detail_service;

    @PostMapping("/add")
    public ResponseEntity<JSONObject> add_course_assist(@RequestBody Map<Object, Object> params, @RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            String course_number = params.get("course_number").toString();
            Course_info course_info = course_service.getInfo(course_number);
            if (course_info == null) {
                return r.error(404);
            }
            int cid = course_info.getCourse_id();
            int tid = teacher_service.getInfo(redisTemplate.opsForValue().get(token)).getTeacher_id();
            List<String> assists = (List<String>) params.get("assists");
            if (!assists.isEmpty()) {
                for (String assist : assists) {
                    int caid = course_assist_service.getLatestID() + 1;
                    Assist_info assist_info = assist_service.getInfo(assist);
                    if (assist_info == null) {
                        return r.error(404);
                    } else {
                        int aid = assist_info.getAssist_id();
                        if (!course_assist_service.addCourseAssist(caid, cid, aid, tid)) {
                            return r.error(10009);
                        }
                    }
                }
            }
            return r.success(200, "添加课程助教", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<JSONObject> delete_course_assist(@RequestBody Map<Object, Object> params, @RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            String course_number = params.get("course_number").toString();
            String assist_phone = params.get("assist_phone").toString();
            Course_info course_info = course_service.getInfo(course_number);
            Assist_info assist_info = assist_service.getInfo(assist_phone);
            Teacher_info teacher_info = teacher_service.getInfo(redisTemplate.opsForValue().get(token));
            if (course_info == null || assist_info == null || teacher_info == null) {
                return r.error(404);
            }
            int cid = course_info.getCourse_id(); // 课程id
            int aid = assist_info.getAssist_id(); // 助教id
            int tid = teacher_info.getTeacher_id(); // 教师id
            // 将所有该助教做最后修改的内容的修改人改为教师
            List<Course_detail> course_details = course_detail_service.showCourseAllDetail(cid);
            // 课程详细内容
            for (Course_detail course_detail : course_details) {
                if (course_detail.getEvent_last_mod_type() == 2 && course_detail.getEvent_last_mod_id() == aid) {
                    course_detail_service.modifyDetailPrincipal(tid, course_detail.getCourse_detail_id());
                }
                // 课程文件内容
                List<Course_document> course_documents = course_doc_service.showAssignCourseDocument(course_detail.getCourse_detail_id());
                for (Course_document course_document : course_documents) {
                    if (course_document.getDoc_uploader_type() == 2 && course_document.getDoc_uploader_id() == aid) {
                        course_doc_service.modifyDocumentPrincipal(tid, course_document.getCourse_doc_id());
                    }
                }
            }
            if (!course_assist_service.ensureAssistUnique(cid, aid)) {
                return r.error(404);
            }
            course_assist_service.deleteAssist(cid, aid);
            return r.success(200, "删除助教信息", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/info")
    public ResponseEntity<JSONObject> get_course_assist_info(@RequestBody Map<Object, Object> params) {
        Returns r = new Returns();
        List<Map<String, Object>> assist_infos = new ArrayList<>();
        try {
            String course_number = params.get("course_number").toString();
            Course_info course_info = course_service.getInfo(course_number);
            if (course_info == null) {
                return r.error(404);
            }
            List<Course_assist> course_assist = course_assist_service.showAssignCourseAssist(course_info.getCourse_id());
            // 选择部分信息放进课程助教栏
            for (Course_assist assist : course_assist) {
                int aid = assist.getAssist_id();
                Assist_info assist_info = assist_service.showAssignCourseAssistInfo(aid);
                Map<String, Object> temp = new HashMap<>();
                temp.put("assist_phone", assist_info.getAssist_phone());
                temp.put("assist_name", assist_info.getAssist_name());
                temp.put("assist_image", assist_info.getAssist_image());
                assist_infos.add(temp);
            }
            return r.success(200, "获取课程助教信息", assist_infos);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }
}
