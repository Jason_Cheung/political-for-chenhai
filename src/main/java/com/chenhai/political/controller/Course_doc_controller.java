package com.chenhai.political.controller;

import com.alibaba.fastjson.JSONObject;
import com.chenhai.political.assist.returns.Returns;
import com.chenhai.political.entity.Course_assist;
import com.chenhai.political.service.*;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;

import static com.chenhai.political.assist.Supports.delete_file;
import static com.chenhai.political.assist.Supports.get_today_date;

@RestController
@RequestMapping("/coursedoc")
@CrossOrigin
public class Course_doc_controller {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private Assist_service assist_service;

    @Autowired
    private Teacher_service teacher_service;

    @Autowired
    private Course_detail_service course_detail_service;

    @Autowired
    private Course_doc_service course_doc_service;

    @Autowired
    private Course_service course_service;

    @Autowired
    private Course_assist_service course_assist_service;

    //文件上传的根目录
    private String uploadPath = File.separator + "home" + File.separator + "ubuntu" + File.separator
            + "Political_Folder" + File.separator + "Course_File";

    // 获取日志生成器
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    // 获取今日日期
    private String TODAY_DATE = get_today_date();

    // 上传目录根地址
    private String UPLOAD_ROOT = uploadPath + File.separator;


    @PostMapping("/add")
    public ResponseEntity<JSONObject> add_course_doc(@RequestParam("FILE") MultipartFile file,    // 要上传的文件
                                                     @RequestParam("Course_number") String course_number,      // 课程周数的ID
                                                     @RequestParam("week") int week,              // 上课周数
                                                     @RequestParam("utype") int utype,            // 用户类型是教师还是助教
                                                     @RequestHeader("token") String token) {
        Returns r = new Returns();
        Map<String, Object> datas = new HashMap<>();
        try {
            int docid = course_doc_service.getLatestID() + 1;
            int course_id = course_service.getInfo(course_number).getCourse_id();
            int detailid = course_detail_service.getWeekInfo(course_id, week).getCourse_detail_id();
            String COURSE_FOLDER = UPLOAD_ROOT + course_number + File.separator; // 课程文件夹
            File dir = new File(COURSE_FOLDER); // 如果该文件夹不存在就新建文件夹
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    throw new IOException();
                }
            }
            String file_name = "WEEK_" + week + "_" + file.getOriginalFilename(); // 新的文件名
            File course_folder = new File(COURSE_FOLDER);
            String file_path = COURSE_FOLDER + file_name;
            // 检查文件是否已经存在
            File checkExistFile = new File(file_path);
            boolean covered = false;
            if (checkExistFile.exists()) {
                if (!checkExistFile.delete()) {
                    return r.error(10005);
                }
                course_doc_service.deleteDocument(file_path);
                covered = true;
            }
            // 检查文件夹是否已经超出可上传的数量
            if (course_folder != null && Objects.requireNonNull(course_folder.listFiles()).length >= 100) {
                return r.error(10004); // 最多上传100个
            } else {

                System.out.println("根目录路径：" + UPLOAD_ROOT);
                System.out.println("课程文件夹路径：" + COURSE_FOLDER);
                System.out.println("文件路径：" + file_path);
                IOUtils.copy(file.getInputStream(), new FileOutputStream(file_path)); // 上传到指定的位置
                // 根据上传的身份类型是老师还是助教来分别写入数据库
                if (utype == 1) { // 如果是老师
                    int tid = teacher_service.getInfo(redisTemplate.opsForValue().get(token)).getTeacher_id(); // 获取老师的ID
                    course_doc_service.addDocumentInfo(docid, detailid, file_path, 1, tid, utype);
                } else { // 如果是助教
                    int aid = assist_service.getInfo(redisTemplate.opsForValue().get(token)).getAssist_id(); // 获取该助教的ID
                    List<Course_assist> result = course_assist_service.getInfo(course_id); // 获取该课程的所有助教
                    boolean contains = false;
                    for (Course_assist assist : result) { // 遍历该课程的所有助教
                        if (assist.getAssist_id() == aid) { // 如果存在该助教
                            contains = true;
                            break;
                        }
                    }
                    if (contains) {
                        course_doc_service.addDocumentInfo(docid, detailid, file_path, 1, aid, utype);
                    } else {
                        return r.error(404);
                    }
                }
            }
            datas.put("fileName", file_name);
            datas.put("remaining", 100 - Objects.requireNonNull(course_folder.listFiles()).length);
            datas.put("covered", covered);
            return r.success(200, "上传课程文件", datas);
        } catch (IndexOutOfBoundsException e) {
            return r.error(404);
        } catch (IOException e) {
            return r.error(10005);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<JSONObject> delete_course_doc(@RequestBody Map<Object, Object> params) {
        Returns r = new Returns();
        try {
            String file_name = params.get("file_name").toString();
            String course_number = params.get("course_number").toString();
            String loc = UPLOAD_ROOT + course_number + File.separator + file_name;
            File file_path = new File(loc);
            delete_file(file_path);
            course_doc_service.deleteDocument(loc);
            return r.success(200, "删除课程文件", file_path);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (FileNotFoundException e) {
            return r.error(404);
        } catch (IOException e) {
            return r.error(10005);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @GetMapping("/download/{course_number}/{file_name}")
    public ResponseEntity<JSONObject> download_course_doc(@PathVariable(value = "course_number") String course_number,
                                                          @PathVariable(value = "file_name") String file_name,
                                                          HttpServletResponse response) {
        Returns r = new Returns();
        try {
//            String course_number = params.get("course_number").toString();
//            String fileName = params.get("file_name").toString();
            // 要下载的文件的全路径名
            String file_path = UPLOAD_ROOT + course_number + File.separator + file_name;
            File file = new File(file_path);
            if (!file.exists()) {
                return r.error(404);
            }
            // 获取文件名
            String filename = file.getName();
            // 添加一次下载记录
            course_doc_service.addDocumentDownloadTimes(course_doc_service.getDocumentInfo(file_path).getCourse_doc_id());
            // 通过流把文件内容写入到客户端
            InputStream fis = new BufferedInputStream(new FileInputStream(file_path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes(), StandardCharsets.ISO_8859_1));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return r.error(10005);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

}
