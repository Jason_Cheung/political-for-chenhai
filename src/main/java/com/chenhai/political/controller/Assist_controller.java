package com.chenhai.political.controller;

import com.alibaba.fastjson.JSONObject;
import com.chenhai.political.assist.returns.Returns;
import com.chenhai.political.entity.Assist_info;
import com.chenhai.political.service.Assist_service;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.chenhai.political.assist.Supports.upload_people_image;

@RestController
@RequestMapping("/assist")
@CrossOrigin
public class Assist_controller {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private Assist_service assist_service;

    @PostMapping("/register")
    public ResponseEntity<JSONObject> register_assist(@RequestParam(value = "assist_phone") String phone,
                                                      @RequestParam(value = "assist_name") String name,
                                                      @RequestParam(value = "assist_password") String pwd,
                                                      @RequestParam(value = "assist_image", required = false) MultipartFile file) {
        Returns r = new Returns();
        try {
            if (assist_service.getInfo(phone) != null) {
                return r.error(10006);
            } else {
                String img_loc = upload_people_image("assist", name, file);
                int assist_id = assist_service.getLatestID() + 1;
                assist_service.registerAssist(assist_id, phone, name, pwd, img_loc);
                r.setMessage("成功");
            }
            return r.success(200, "助教注册", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (IOException e) {
            return r.error(10005);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<JSONObject> login_admin(@RequestParam("phone") String phone,
                                                  @RequestParam("password") String password,
                                                  @RequestHeader(value = "token", required = false) String token) {
        Returns r = new Returns();
        try {
            // 账号密码错误
            if (!assist_service.checkLogin(phone, password)) {
                return r.error(401);
            }
            // 账号密码正确
            if (token != null && Objects.equals(redisTemplate.opsForValue().get(token), phone)) {
                redisTemplate.expire(token, 600, TimeUnit.SECONDS);
                r.setMessage("已经登录过");
                r.setToken(token);
            } else {
                // 添加token
                String new_token = UUID.randomUUID().toString().replaceAll("-", "");
                redisTemplate.opsForValue().set(new_token, phone, 600, TimeUnit.SECONDS);
                r.setMessage("成功");
                r.setToken(new_token);
            }
            return r.success(200, "助教登录", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/logout")
    public ResponseEntity<JSONObject> logout_assist(@RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            redisTemplate.delete(token);
            r.setMessage("退出登录成功，token已失效");
            return r.success(200, "助教退出登录", null);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @ApiOperation("更新助教信息")
    @PostMapping("/modify")
    public ResponseEntity<JSONObject> modify_assist_info(@RequestParam(value = "assist_phone") String new_phone,
                                                         @RequestParam(value = "assist_name") String name,
                                                         @RequestParam(value = "assist_password") String pwd,
                                                         @RequestParam(value = "assist_image", required = false) MultipartFile file,
                                                         @RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            String old_phone = redisTemplate.opsForValue().get(token);
            int aid = assist_service.getInfo(old_phone).getAssist_id();
            if (assist_service.getInfo(old_phone) == null) {
                return r.error(404);
            } else if (!old_phone.equals(new_phone) && assist_service.getInfo(new_phone) != null) {
                return r.error(10006);
            } else {
                String img_loc = upload_people_image("assist", name, file);
                assist_service.modifyAssist(aid, new_phone, name, pwd, img_loc);
                redisTemplate.opsForValue().set(token, new_phone, 600, TimeUnit.SECONDS); // Token续费
                r.setMessage("成功");
            }
            return r.success(200, "助教更新个人信息", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/info")
    public ResponseEntity<JSONObject> get_assist_info(@RequestHeader("token") String token) {
        Returns r = new Returns();
        try{
            String phone = redisTemplate.opsForValue().get(token);
            Assist_info assist_info = assist_service.getInfo(phone);
            if (assist_info == null) {
                return r.error(404);
            }
            return r.success(200, "助教获取个人信息", assist_info);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

}
