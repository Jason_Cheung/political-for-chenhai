package com.chenhai.political.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chenhai.political.assist.returns.Returns;
import com.chenhai.political.entity.Assist_info;
import com.chenhai.political.entity.Course_assist;
import com.chenhai.political.entity.Course_info;
import com.chenhai.political.entity.Teacher_info;
import com.chenhai.political.service.Assist_service;
import com.chenhai.political.service.Course_assist_service;
import com.chenhai.political.service.Course_service;
import com.chenhai.political.service.Teacher_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/course")
@CrossOrigin
public class Course_controller {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private Assist_service assist_service;

    @Autowired
    private Teacher_service teacher_service;

    @Autowired
    private Course_service course_service;

    @Autowired
    private Course_assist_service course_assist_service;

    @PostMapping("/add")
    public ResponseEntity<JSONObject> add_course(@RequestBody Map<Object, Object> params, @RequestHeader("token") String token) {
        Returns r = new Returns();
        List<String> not_exist_assist = new ArrayList<>();
        try {
            Teacher_info teacher_info = teacher_service.getInfo(redisTemplate.opsForValue().get(token));
            if (teacher_info == null) {
                return r.error(403);
            }
            String number = params.get("course_number").toString();
            String name = params.get("course_name").toString();
            if (course_service.getInfo(number) != null) {
                return r.error(10007);
            }
            String semester = params.get("course_semester").toString();
            int tid = teacher_info.getTeacher_id(); // 教师ID主键
            String intro = params.get("course_intro").toString();
            int cid = course_service.getLatestID() + 1; // 课程ID主键
            course_service.addCourseInfo(cid, number, name, semester, tid, intro);
            // 增加课程助教
            List<String> assists = (List<String>) params.get("assists");
            if (!assists.isEmpty()) {
                for (String assist : assists) {
                    int caid = course_assist_service.getLatestID() + 1;
                    Assist_info assist_info = assist_service.getInfo(assist);
                    if (assist_info == null) {
                        not_exist_assist.add(assist);
                    } else {
                        int aid = assist_info.getAssist_id();
                        if (!course_assist_service.addCourseAssist(caid, cid, aid, tid)) {
                            return r.error(10009);
                        }
                    }
                }
            }
            return r.success(200, "添加课程及助教，添加失败的助教见data", not_exist_assist);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/modify")
    public ResponseEntity<JSONObject> modify_course(@RequestBody Map<Object, Object> params, @RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            String name = params.get("name").toString();
            String number = params.get("number").toString();
            Course_info course_info = course_service.getInfo(number);
            if (course_info == null) {
                return r.error(404);
            }
            int course_id = course_info.getCourse_id();
            String semester = params.get("semester").toString();
            String intro = params.get("intro").toString();
            int utype = Integer.parseInt(params.get("utype").toString());
            int tid = course_info.getCourse_teacher_id();
            if (utype == 1) { // 如果是老师
                course_service.modifyCourseInfo(course_id, number, name, semester, intro, tid, 1);
            } else { // 如果是助教
                int aid = assist_service.getInfo(redisTemplate.opsForValue().get(token)).getAssist_id();
                List<Course_assist> result = course_assist_service.getInfo(course_id); // 获取该课程的所有助教
                boolean contains = false;
                for (Course_assist assist : result) { // 遍历该课程的所有助教
                    if (assist.getAssist_id() == aid) { // 如果存在该助教
                        contains = true;
                        break;
                    }
                }
                if (contains) {
                    course_service.modifyCourseInfo(course_id, number, name, semester, intro, aid, 2);
                } else {
                    return r.error(404);
                }
            }
            return r.success(200, "修改课程信息", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    /* 获取教师负责的所有课程 */
    @PostMapping("/info")
    public ResponseEntity<JSONObject> get_course_info(@RequestHeader("token") String token) {
        Returns r = new Returns();
        List<Map<String, Object>> course_infos = new ArrayList<>();
        try {
            String phone = redisTemplate.opsForValue().get(token);
            Teacher_info teacher_info = teacher_service.getInfo(phone);
            if (teacher_info == null) {
                 return r.error(404);
            }
            List<Course_info> courses = course_service.getTeacherCourseInfo(teacher_info.getTeacher_id());
            for (Course_info course_info : courses) {
                Map<String, Object> temp = new HashMap<>();
                temp.put("course_number", course_info.getCourse_number());
                temp.put("course_name", course_info.getCourse_name());
                temp.put("course_semester", course_info.getCourse_semester());
                temp.put("course_introduce", course_info.getCourse_introduce());
                temp.put("course_add_time", course_info.getCourse_add_time());
                temp.put("course_mod_time", course_info.getCourse_mod_time());
                temp.put("course_last_modifier", course_info.getCourse_last_mod_type() == 1 ?
                    teacher_info.getTeacher_name() : assist_service.showAssignCourseAssistInfo(course_info.getCourse_last_mod_id()));
                course_infos.add(temp);
            }
            return r.success(200, "获取教师负责的所有课程", course_infos);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }
}
