package com.chenhai.political.controller;

import com.alibaba.fastjson.JSONObject;
import com.chenhai.political.assist.returns.Returns;
import com.chenhai.political.entity.*;
import com.chenhai.political.service.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/political")
@CrossOrigin
public class Shows_controller {
    @Autowired
    private Teacher_service teacher_service;

    @Autowired
    private Assist_service assist_service;

    @Autowired
    private Course_service course_service;

    @Autowired
    private Course_assist_service course_assist_service;

    @Autowired
    private Course_doc_service course_doc_service;

    @Autowired
    private Course_detail_service course_detail_service;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @GetMapping("/main")
    public ResponseEntity<JSONObject> show_main_page() {
        Returns r = new Returns();
        Map<String, Object> datas = new HashMap<>();
        List<Map<String, Object>> courses_info = new ArrayList<>();
        List<Map<String, Object>> teachers_info = new ArrayList<>();
        try {
            List<Teacher_info> teachers = teacher_service.showAllTeachers();
            List<Course_info> courses = course_service.showAllCourses();
            for (Teacher_info teacher_info : teachers) {
                Map<String, Object> temp = new HashMap<>();
                temp.put("teacher_phone", teacher_info.getTeacher_phone());
                temp.put("teacher_name", teacher_info.getTeacher_name());
                temp.put("teacher_image", teacher_info.getTeacher_image());
                teachers_info.add(temp);
            }
            for (Course_info course_info : courses) {
                Map<String, Object> temp = new HashMap<>();
                temp.put("course_number", course_info.getCourse_number());
                temp.put("course_name", course_info.getCourse_name());
                temp.put("course_semester", course_info.getCourse_semester());
                temp.put("course_teacher", teacher_service.getInfoByID(course_info.getCourse_teacher_id()).getTeacher_name());
                courses_info.add(temp);
            }
            datas.put("teachers", teachers_info);
            datas.put("courses", courses_info);
            return r.success(200, "获取主界面资料", datas);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @GetMapping("/course/{number}")
    public ResponseEntity<JSONObject> show_course_page(@PathVariable(value = "number") String course_number) {
        Returns r = new Returns();
        Map<String, Object> datas = new HashMap<>();
        Map<String, Object> basic_info = new HashMap<>();
        List<Map<String, Object>> assist_infos = new ArrayList<>();
        List<Map<String, Object>> detail_infos = new ArrayList<>();
        try {
            Course_info course_info = course_service.showAssignCourse(course_number);
            if (course_info == null) {
                return r.error(404);
            }
            String teacher_name = teacher_service.getInfoByID(course_info.getCourse_teacher_id()).getTeacher_name();
            basic_info.put("course_number", course_info.getCourse_number());
            basic_info.put("course_name", course_info.getCourse_name());
            basic_info.put("course_semester", course_info.getCourse_semester());
            basic_info.put("course_teacher_name", teacher_name);
            basic_info.put("course_introduce", course_info.getCourse_introduce());
            List<Course_assist> course_assist = course_assist_service.showAssignCourseAssist(course_info.getCourse_id());
            // 选择部分信息放进课程助教栏
            for (Course_assist assist : course_assist) {
                int aid = assist.getAssist_id();
                Assist_info assist_info = assist_service.showAssignCourseAssistInfo(aid);
                Map<String, Object> temp = new HashMap<>();
                temp.put("assist_phone", assist_info.getAssist_phone());
                temp.put("assist_name", assist_info.getAssist_name());
                temp.put("assist_image", assist_info.getAssist_image());
                assist_infos.add(temp);
            }
            // 选择部分信息放进课程详细信息栏
            List<Course_detail> course_details = course_detail_service.showCourseAllDetail(course_info.getCourse_id());
            for (Course_detail course_detail : course_details) {
                Map<String, Object> temp = new HashMap<>();
                temp.put("course_week", course_detail.getCourse_week());
                temp.put("course_date", course_detail.getCourse_date());
                temp.put("course_details", course_detail.getCourse_details());
                temp.put("course_events", course_detail.getCourse_events());
                temp.put("course_event_type", course_detail.getEvent_type());
                temp.put("course_deadline", course_detail.getEvent_deadline());
                temp.put("course_add_time", course_detail.getEvent_add_time());
                temp.put("course_mod_time", course_detail.getEvent_mod_time());
                temp.put("course_detail_last_modifier", course_detail.getEvent_last_mod_type() == 1 ? teacher_name :
                        assist_service.showAssignCourseAssistInfo(course_detail.getEvent_last_mod_id()).getAssist_name());
                // 选择部分信息放进课程文件栏
                List<Course_document> Course_documents = course_doc_service.showAssignCourseDocument(course_detail.getCourse_detail_id());
                List<Map<String, Object>> docs = new ArrayList<>();
                for (Course_document document : Course_documents) {
                    Map<String, Object> course_document_info_temp = new HashMap<>();
                    course_document_info_temp.put("doc_location", document.getDoc_loc().split("/")[6]);
                    course_document_info_temp.put("doc_add_time", document.getDoc_add_time());
                    course_document_info_temp.put("doc_uploader", document.getDoc_uploader_type() == 1 ? teacher_name :
                            assist_service.showAssignCourseAssistInfo(document.getDoc_uploader_id()).getAssist_name());
                    docs.add(course_document_info_temp);
                }
                temp.put("course_documents", docs);
                detail_infos.add(temp);
            }
            datas.put("Base Info", basic_info);
            datas.put("Assists", assist_infos);
            datas.put("Details", detail_infos);
            return r.success(200, "获取课程信息", datas);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @GetMapping("/test")
    public ResponseEntity<JSONObject> test() {
        Returns r = new Returns();
        Map<String, Object> datas = new HashMap<>();
        try {
            Assist_info temp = assist_service.showAssignCourseAssistInfo(1);
            datas.put("test", temp);
            return r.success(200, "success", datas);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }
}
