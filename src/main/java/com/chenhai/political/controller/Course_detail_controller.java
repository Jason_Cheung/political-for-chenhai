package com.chenhai.political.controller;

import com.alibaba.fastjson.JSONObject;
import com.chenhai.political.assist.returns.Returns;
import com.chenhai.political.entity.Course_assist;
import com.chenhai.political.entity.Course_detail;
import com.chenhai.political.entity.Course_document;
import com.chenhai.political.entity.Course_info;
import com.chenhai.political.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.chenhai.political.assist.Supports.*;

@RestController
@RequestMapping("/coursedetail")
@CrossOrigin
public class Course_detail_controller {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private Assist_service assist_service;

    @Autowired
    private Teacher_service teacher_service;

    @Autowired
    private Course_detail_service course_detail_service;

    @Autowired
    private Course_service course_service;

    @Autowired
    private Course_assist_service course_assist_service;

    @Autowired
    private Course_doc_service course_doc_service;

    @PostMapping("/add")
    public ResponseEntity<JSONObject> add_course_detail(@RequestBody Map<Object, Object> params, @RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            int cdid = course_detail_service.getLatestID() + 1; // 主键ID
            String number = params.get("course_number").toString();
            Course_info course_info = course_service.getInfo(number);
            if (course_info == null) {
                return r.error(404);
            }
            int cid = course_info.getCourse_id(); // 课程ID，根据传入的课程名和课程号得出
            int week = (int) params.get("week"); // 第几周
            if (course_detail_service.ensureAddWeekUnique(cid, week)) {
                return r.error(10008);
            }
            Date date = params.get("date").toString().equals("") ? null : string_to_date(params.get("date").toString()); // 上课日期
            String details = params.get("details").toString().equals("") ? null : params.get("details").toString(); // 课程本周详细信息
            String events = params.get("events").toString().equals("") ? null : params.get("events").toString(); // 课程本周事件
            int type = (int) params.get("type"); // 本周课程类型（正常、考试、停课）
            Timestamp deadline = params.get("deadline").toString().equals("") ? null : string_to_timestamp(params.get("deadline").toString()); // 截止时间
            int utype = (int) params.get("utype"); // 编辑者的种类
            if (utype == 1) { // 如果是老师
                int tid = teacher_service.getInfo(redisTemplate.opsForValue().get(token)).getTeacher_id(); // 获取老师的ID
                course_detail_service.addDetailInfo(cdid, cid, week, date, details, events, type, deadline, tid, utype);
            } else { // 如果是助教
                int aid = assist_service.getInfo(redisTemplate.opsForValue().get(token)).getAssist_id(); // 获取该助教的ID
                List<Course_assist> result = course_assist_service.getInfo(cid); // 获取该课程的所有助教
                boolean contains = false;
                for (Course_assist assist : result) { // 遍历该课程的所有助教
                    if (assist.getAssist_id() == aid) { // 如果存在该助教
                        contains = true;
                        break;
                    }
                }
                if (contains) {
                    course_detail_service.addDetailInfo(cdid, cid, week, date, details, events, type, deadline, aid, utype);
                } else {
                    return r.error(404);
                }
            }
            return r.success(200, "添加课程详细信息", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }

    }

    @PostMapping("/modify")
    public ResponseEntity<JSONObject> modify_course_detail(@RequestBody Map<Object, Object> params, @RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            String number = params.get("course_number").toString();
            Course_info course_info = course_service.getInfo(number);
            if (course_info == null) {
                return r.error(404);
            }
            int cid = course_info.getCourse_id(); // 课程ID，根据传入的课程名和课程号得出
            int week = (int) params.get("week"); // 第几周
            Course_detail course_detail = course_detail_service.getWeekInfo(cid, week);
            if (course_detail == null) {
                return r.error(404);
            }
            int cdid = course_detail.getCourse_detail_id();
            System.out.println("cdid = " + cdid);
            Date date = params.get("date").toString().equals("") ? null : string_to_date(params.get("date").toString()); // 上课日期
            String details = params.get("details").toString().equals("") ? null : params.get("details").toString(); // 课程本周详细信息
            String events = params.get("events").toString().equals("") ? null : params.get("events").toString(); // 课程本周事件
            int type = (int) params.get("type"); // 本周课程类型（正常、考试、停课）
            Timestamp deadline = params.get("deadline").toString().equals("") ? null : string_to_timestamp(params.get("deadline").toString()); // 截止时间
            int utype = (int) params.get("utype"); // 编辑者的种类
            if (utype == 1) { // 如果是老师
                int tid = teacher_service.getInfo(redisTemplate.opsForValue().get(token)).getTeacher_id(); // 获取老师的ID
                course_detail_service.modifyDetailInfo(cdid, week, date, details, events, type, deadline, tid, utype);
            } else { // 如果是助教
                int aid = assist_service.getInfo(redisTemplate.opsForValue().get(token)).getAssist_id(); // 获取该助教的ID
                List<Course_assist> result = course_assist_service.getInfo(cid); // 获取该课程的所有助教
                boolean contains = false;
                for (Course_assist assist : result) { // 遍历该课程的所有助教
                    if (assist.getAssist_id() == aid) { // 如果存在该助教
                        contains = true;
                        break;
                    }
                }
                if (contains) {
                    course_detail_service.modifyDetailInfo(cdid, week, date, details, events, type, deadline, aid, utype);
                } else {
                    return r.error(404);
                }
            }
            return r.success(200, "修改课程详细信息", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<JSONObject> delete_course_detail(@RequestBody Map<Object, Object> params, @RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            String number = params.get("course_number").toString();
            Course_info course_info = course_service.getInfo(number);
            if (course_info == null) {
                return r.error(404);
            }
            int cid = course_info.getCourse_id(); // 课程ID，根据传入的课程名和课程号得出
            int week = (int) params.get("week"); // 第几周
            // 找出所有该周的所有文件，然后将其删除
            List<Course_document> course_documents = course_doc_service.showAssignCourseDocument(course_detail_service.getWeekInfo(cid, week).getCourse_detail_id());
            for (Course_document course_document : course_documents) {
                File file_path = new File(course_document.getDoc_loc());
                delete_file(file_path);
                course_doc_service.deleteDocument(String.valueOf(file_path));
            }
            course_detail_service.deleteDetailInfo(cid, week);
            return r.success(200, "删除课程详细信息，并删除所有该周的文件", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/info")
    public ResponseEntity<JSONObject> get_course_detail_info(@RequestBody Map<Object, Object> params) {
        Returns r = new Returns();
        List<Map<String, Object>> detail_infos = new ArrayList<>();
        try {
            String course_number = params.get("course_number").toString();
            Course_info course_info = course_service.getInfo(course_number);
            if (course_info == null) {
                return r.error(404);
            }
            String teacher_name = teacher_service.getInfoByID(course_info.getCourse_teacher_id()).getTeacher_name();
            List<Course_detail> course_details = course_detail_service.showCourseAllDetail(course_info.getCourse_id());
            for (Course_detail course_detail : course_details) {
                Map<String, Object> temp = new HashMap<>();
                temp.put("course_week", course_detail.getCourse_week());
                temp.put("course_date", course_detail.getCourse_date());
                temp.put("course_details", course_detail.getCourse_details());
                temp.put("course_events", course_detail.getCourse_events());
                temp.put("course_event_type", course_detail.getEvent_type());
                temp.put("course_deadline", course_detail.getEvent_deadline());
                temp.put("course_add_time", course_detail.getEvent_add_time());
                temp.put("course_mod_time", course_detail.getEvent_mod_time());
                temp.put("course_detail_last_modifier", course_detail.getEvent_last_mod_type() == 1 ? teacher_name :
                        assist_service.showAssignCourseAssistInfo(course_detail.getEvent_last_mod_id()).getAssist_name());
                // 选择部分信息放进课程文件栏
                List<Course_document> Course_documents = course_doc_service.showAssignCourseDocument(course_detail.getCourse_detail_id());
                List<Map<String, Object>> docs = new ArrayList<>();
                for (Course_document document : Course_documents ){
                    Map<String, Object> course_document_info_temp = new HashMap<>();
                    course_document_info_temp.put("doc_location", document.getDoc_loc());
                    course_document_info_temp.put("doc_add_time", document.getDoc_add_time());
                    course_document_info_temp.put("doc_uploader", document.getDoc_uploader_type() == 1 ? teacher_name :
                            assist_service.showAssignCourseAssistInfo(document.getDoc_uploader_id()).getAssist_name());
                    docs.add(course_document_info_temp);
                }
                temp.put("course_documents", docs);
                detail_infos.add(temp);
            }
            return r.success(200, "获取课程详细信息", detail_infos);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }
}
