package com.chenhai.political.controller;

import com.alibaba.fastjson.JSONObject;
import com.chenhai.political.assist.returns.Returns;
import com.chenhai.political.entity.Assist_info;
import com.chenhai.political.entity.Teacher_info;
import com.chenhai.political.service.Teacher_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.chenhai.political.assist.Supports.upload_people_image;

@RestController
@RequestMapping("/teacher")
@CrossOrigin
public class Teacher_controller {
    @Autowired
    private Teacher_service teacher_service;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @PostMapping("/register")
    public ResponseEntity<JSONObject> register_teacher(@RequestParam(value = "teacher_phone") String phone,
                                                       @RequestParam(value = "teacher_name") String name,
                                                       @RequestParam(value = "teacher_password") String pwd,
                                                       @RequestParam(value = "teacher_image", required = false) MultipartFile file) {
        Returns r = new Returns();
        try {
            if (teacher_service.getInfo(phone) != null) {
                return r.error(10006);
            } else {
                String img_loc = upload_people_image("teacher", name, file);
                int teacher_id = teacher_service.getLatestID() + 1;
                teacher_service.registerTeacher(teacher_id, phone, name, pwd, img_loc);
                r.setMessage("成功");
            }
            return r.success(200, "教师注册", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (IOException e) {
            return r.error(10005);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<JSONObject> login_teacher(@RequestParam("phone") String phone,
                                                    @RequestParam("password") String password,
                                                    @RequestHeader(value = "token", required = false) String token) {
        Returns r = new Returns();
        try {
            // 账号密码错误
            if (!teacher_service.checkLogin(phone, password)) {
                return r.error(401);
            }
            // 账号密码正确
            if (token != null && Objects.equals(redisTemplate.opsForValue().get(token), phone)) {
                redisTemplate.expire(token, 600, TimeUnit.SECONDS);
                r.setMessage("已经登录过");
                r.setToken(token);
            } else {
                // 添加token
                String new_token = UUID.randomUUID().toString().replaceAll("-", "");
                redisTemplate.opsForValue().set(new_token, phone, 600, TimeUnit.SECONDS);
                r.setMessage("成功");
                r.setToken(new_token);
            }
            return r.success(200, "教师登录", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/logout")
    public ResponseEntity<JSONObject> logout_teacher(@RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            redisTemplate.delete(token);
            r.setMessage("退出登录成功，token已失效");
            return r.success(200, "教师退出登录", null);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }


    @PostMapping("/modify")
    public ResponseEntity<JSONObject> modify_teacher_info(@RequestParam(value = "teacher_phone") String new_phone,
                                                          @RequestParam(value = "teacher_name") String name,
                                                          @RequestParam(value = "teacher_password") String pwd,
                                                          @RequestParam(value = "teacher_image", required = false) MultipartFile file,
                                                          @RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            String old_phone = redisTemplate.opsForValue().get(token);
            int tid = teacher_service.getInfo(old_phone).getTeacher_id();
            if (teacher_service.getInfo(old_phone) == null) {
                return r.error(404);
            } else if (!old_phone.equals(new_phone) && teacher_service.getInfo(new_phone) != null) {
                return r.error(10006);
            } else {
                String img_loc = upload_people_image("teacher", name, file);
                teacher_service.modifyTeacher(tid, new_phone, name, pwd, img_loc);
                redisTemplate.opsForValue().set(token, new_phone, 600, TimeUnit.SECONDS); // Token续费
                r.setMessage("成功");
            }
            return r.success(200, "教师更新个人信息", null);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }

    @PostMapping("/info")
    public ResponseEntity<JSONObject> get_teacher_info(@RequestHeader("token") String token) {
        Returns r = new Returns();
        try {
            String phone = redisTemplate.opsForValue().get(token);
            Teacher_info teacher_info = teacher_service.getInfo(phone);
            if (teacher_info == null) {
                return r.error(404);
            }
            return r.success(200, "教师获取个人信息", teacher_info);
        } catch (NullPointerException e) {
            return r.error(400);
        } catch (Exception e) {
            e.printStackTrace();
            return r.error(500);
        }
    }
}
