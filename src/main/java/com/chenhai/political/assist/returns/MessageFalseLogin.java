package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

public class MessageFalseLogin implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("Incorrect Account or Password, or No Register");
//        r.setStatus(UNAUTHORIZED);
        r.setStatus(OK);
    }
}
