package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

public class MessageIllegalParameter implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("Illegal Parameter");
//        r.setStatus(BAD_REQUEST);
        r.setStatus(OK);
    }
}
