package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

public class MessageInvalidToken implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("No Token or The Token Is Invalid");
//        r.setStatus(UNAUTHORIZED);
        r.setStatus(OK);
    }
}
