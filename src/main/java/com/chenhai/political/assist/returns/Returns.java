package com.chenhai.political.assist.returns;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.HttpStatus.OK;

@Data
public class Returns {
    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Object data;

    @ApiModelProperty(value = "返回HTTP状态码")
    private HttpStatus status;

    @ApiModelProperty(value = "操作行为")
    private String operation;

    @ApiModelProperty(value = "token")
    private String token;

    public Returns() {
    }

    public ResponseEntity<JSONObject> error(int c) {
        this.code = c;
        switch (code) {
            case 400:
                new MessageIncorrectJSON().setMessageAndStatus(this); // JSON格式有误
                break;
            case 401:
                new MessageFalseLogin().setMessageAndStatus(this); // 账号或密码错误
                break;
            case 403:
                new MessageInsufficientPermissions().setMessageAndStatus(this); // 权限不足导致的拒绝
                break;
            case 404:
                new MessageRequestNotFound().setMessageAndStatus(this); // 请求的资源不存在
                break;
            case 500:
                new MessageServerError().setMessageAndStatus(this); // 服务器内部错误
                break;
            case 10001:
                new MessageInvalidToken().setMessageAndStatus(this); // Token失效或不存在
                break;
            case 10002:
                new MessageIllegalParameter().setMessageAndStatus(this); // 非法参数
                break;
            case 10003:
                new MessageIllegalSearch().setMessageAndStatus(this); // 非法搜索
                break;
            case 10004:
                new MessageUploadLimitation().setMessageAndStatus(this); // 上传图片限制
                break;
            case 10005:
                new MessageUploadIOE().setMessageAndStatus(this); // 上传IO请求错误
                break;
            case 10006:
                new MessageAccountExists().setMessageAndStatus(this); // 账号已存在
                break;
            case 10007:
                new MessageCourseExists().setMessageAndStatus(this); // 课程已经存在
                break;
            case 10008:
                new MessageCourseDetailExists().setMessageAndStatus(this); // 课程详细信息已经存在
                break;
            case 10009:
                new MessageCourseAssistExists().setMessageAndStatus(this); // 添加课程的助教已经存在
                break;
            default:
                new MessageUnrecognizedCode().setMessageAndStatus(this); // 其他无法识别的状态码
                break;
        }
        JSONObject j = new JSONObject();
        j.put("code", code);
        j.put("success", false);
        j.put("message", message);
        return new ResponseEntity<>(j, status);
    }

    public ResponseEntity<JSONObject> success(int c, String o, Object d) {
        this.code = c;
        this.operation = o;
        JSONObject j = new JSONObject();
        if (d != null) {
            this.data = d;
            j.put("data", data);
        }
        if (token != null) {
            j.put("obj", token);
        }
        j.put("code", code);
        j.put("success", true);
        j.put("operation", operation);
        return new ResponseEntity<>(j, OK);
    }

    public JSONObject formatter(Returns r) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", r.getCode());
        jsonObject.put("message", r.getMessage());
        if (r.getCode() != 200) {
            jsonObject.put("success", false);
        }
        jsonObject.put("operation", r.getOperation());
        return jsonObject;
    }
}
