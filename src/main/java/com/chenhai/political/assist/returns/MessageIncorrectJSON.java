package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

public class MessageIncorrectJSON implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("Unrecognized JSON Key or JSON Format Error");
//        r.setStatus(BAD_REQUEST);
        r.setStatus(OK);
    }
}
