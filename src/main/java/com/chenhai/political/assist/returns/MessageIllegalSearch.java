package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

public class MessageIllegalSearch implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("Illegal Search");
//        r.setStatus(BAD_REQUEST);
        r.setStatus(OK);
    }
}
