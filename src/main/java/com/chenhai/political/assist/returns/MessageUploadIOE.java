package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

public class MessageUploadIOE implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("Upload Failed Due To IOException");
//        r.setStatus(INTERNAL_SERVER_ERROR);
        r.setStatus(OK);
    }
}
