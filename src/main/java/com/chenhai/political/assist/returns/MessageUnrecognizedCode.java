package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

public class MessageUnrecognizedCode implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("No Such Response Code");
        r.setStatus(INTERNAL_SERVER_ERROR);
//        r.setStatus(OK);
    }
}
