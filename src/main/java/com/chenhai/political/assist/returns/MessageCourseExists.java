package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

public class MessageCourseExists implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("Course Already Exists");
//        r.setStatus(FORBIDDEN);
        r.setStatus(OK);
    }
}
