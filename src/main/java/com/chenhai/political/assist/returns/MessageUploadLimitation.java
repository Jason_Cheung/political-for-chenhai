package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

public class MessageUploadLimitation implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("Upload Failed Due To Quantity Limitation");
//        r.setStatus(FORBIDDEN);
        r.setStatus(OK);
    }
}
