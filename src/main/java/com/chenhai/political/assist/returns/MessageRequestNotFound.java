package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

public class MessageRequestNotFound implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("The Requested Content Does Not Exist (Resources, Infos, Account)");
//        r.setStatus(NOT_FOUND);
        r.setStatus(OK);
    }
}
