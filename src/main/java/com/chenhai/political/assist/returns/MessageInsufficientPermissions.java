package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

public class MessageInsufficientPermissions implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("Permission Denied");
//        r.setStatus(FORBIDDEN);
        r.setStatus(OK);
    }
}
