package com.chenhai.political.assist.returns;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

public class MessageServerError implements ReturnMessageAndCode {
    @Override
    public void setMessageAndStatus(Returns r) {
        r.setMessage("Internal Server Error");
//        r.setStatus(INTERNAL_SERVER_ERROR);
        r.setStatus(OK);
    }
}
