package com.chenhai.political.assist;

import com.alibaba.fastjson.JSONObject;
import com.chenhai.political.assist.returns.Returns;
import com.chenhai.political.service.Course_doc_service;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;

public class Supports {

    @Autowired
    private Course_doc_service course_doc_service;

    public static String getNowTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        return sdf.format(new Date());
    }

    public static String get_random_number(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }

    public static String get_today_date() {
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        ft.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));  // 设置北京时区
        return ft.format(date);
    }

    public static Timestamp string_to_timestamp(String timestamp) {
        Timestamp result = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //先将字符创转化为date
            Date date = simpleDateFormat.parse(timestamp);
            //然后将date转化为timestamp
            result = new Timestamp(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Timestamp string_to_timestamp_without_time(String timestamp) {
        Timestamp result = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            //先将字符创转化为date
            Date date = simpleDateFormat.parse(timestamp);
            //然后将date转化为timestamp
            result = new Timestamp(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    // 将String转为Time类型
    public static Time string_to_time(String time) {
        Time result = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            //先将字符创转化为date
            Date date = simpleDateFormat.parse(time);
            //然后将date转化为time
            result = new Time(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static java.sql.Date string_to_date(String timestamp) {
        java.util.Date date = null;
        java.sql.Date result = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            //先将字符创转化为date
            date = simpleDateFormat.parse(timestamp);
            result = new java.sql.Date(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean token_install(@RequestHeader("token") String token, Returns r, String phone, boolean b, StringRedisTemplate redisTemplate) {
        if (b) {
            String token_value = redisTemplate.opsForValue().get(token);
            if (token_value != null && Objects.equals(token_value, phone)) {
                redisTemplate.expire(token, 600, TimeUnit.SECONDS);
                r.setToken(token);
                r.setMessage("已经登录过");
            } else {
                // 添加token
                String new_token = UUID.randomUUID().toString().replaceAll("-", "");
                redisTemplate.opsForValue().set(new_token, phone, 600, TimeUnit.SECONDS);
                r.setToken(new_token);
                r.setMessage("成功");
            }
        } else {
            return true;
        }
        return false;
    }

    // 上传头像到服务器，参数一：人物类型（教师or助教），参数二：姓名，参数三：文件
    public static String upload_people_image(String type, String name, MultipartFile file) throws IOException {
        //文件上传的根目录
        String uploadPath = File.separator + "home" + File.separator + "ubuntu" + File.separator
                + "Political_Folder" + File.separator + "Avatars" + File.separator + type + File.separator; // 服务器环境
//        String uploadPath = "D:" + File.separator + "test_folder" + File.separator
//                + "Avatars" + File.separator + type + File.separator; // 本地环境
        File dir = new File(uploadPath); // 如果该文件夹不存在就新建文件夹
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                throw new IOException();
            }
        }
        // 如果使用isEmpty()方法判断空会报NullPointerException
        if (file == null) {
            return uploadPath + "default.png";
        }
        String suffix = Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf(".")); // 获取后缀
        String File_name = name + "_" + UUID.randomUUID() + suffix; // 修改文件名
        String file_path = uploadPath + File_name; // 文件最终路径
        IOUtils.copy(file.getInputStream(), new FileOutputStream(file_path)); // 上传到指定的位置
        return file_path;
    }

    public static void delete_file(File file) throws IOException {
        if (!file.exists()) {
            throw new FileNotFoundException();
        }
        if (!file.delete()) {
            throw new IOException();
        }
    }
}
