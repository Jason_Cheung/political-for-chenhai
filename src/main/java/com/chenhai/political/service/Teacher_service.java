package com.chenhai.political.service;

import com.chenhai.political.entity.Teacher_info;
import com.chenhai.political.mapper.Teacher_info_mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class Teacher_service {
    @Resource
    private Teacher_info_mapper teacher_info_mapper;

    public Teacher_info getInfo(String phone) {
        return teacher_info_mapper.getInfo(phone);
    }

    public int getLatestID() {
        return teacher_info_mapper.getLatestID();
    }

    public void registerTeacher(int id, String phone, String name, String pwd, String img_loc) {
        teacher_info_mapper.registerTeacher(id, phone, name, pwd, img_loc);
    }

    public void modifyTeacher(int id, String phone, String name, String pwd, String img_loc) {
        teacher_info_mapper.modifyTeacher(id, phone, name, pwd, img_loc);
    }

    public Boolean checkLogin(String phone, String pwd) {
        return teacher_info_mapper.checkLogin(phone, pwd);
    }

    public List<Teacher_info> showAllTeachers() {
        return teacher_info_mapper.showAllTeachers();
    }

    public Teacher_info getInfoByID(int id){
        return teacher_info_mapper.getInfoByID(id);
    }
}
