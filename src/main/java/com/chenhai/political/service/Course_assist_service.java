package com.chenhai.political.service;

import com.chenhai.political.entity.Assist_info;
import com.chenhai.political.entity.Course_assist;
import com.chenhai.political.mapper.Course_assist_mapper;
import com.chenhai.political.mapper.Teacher_info_mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class Course_assist_service {
    @Resource
    private Course_assist_mapper course_assist_mapper;

    public List<Course_assist> getInfo(int cid) {
        return course_assist_mapper.getInfo(cid);
    }

    public boolean addCourseAssist(int caid, int cid, int aid, int tid) {
        // 如果返回的条数为1则返回true，代表要该课程的助教已经存在。如果为0则返回false，可以给该课程添加助教
        if (ensureAssistUnique(cid, aid)) {
            return false;
        }
        course_assist_mapper.addCourseAssist(caid, cid, aid, tid);
        return true;
    }

    public void deleteAssist(int cid, int aid) {
        course_assist_mapper.deleteAssist(cid, aid);
    }

    public int getLatestID() {
        return course_assist_mapper.getLatestID();
    }

    public boolean ensureAssistUnique(int cid, int aid) {
        // 如果返回的条数为1则返回true，代表要该课程的助教已经存在。如果为0则返回false，代表该课程没有该助教
        return course_assist_mapper.ensureAssistUnique(cid, aid);
    }

    public List<Course_assist> showAssignCourseAssist(int cid) {
        return course_assist_mapper.showAssignCourseAssist(cid);
    }
}
