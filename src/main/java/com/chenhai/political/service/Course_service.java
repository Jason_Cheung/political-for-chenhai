package com.chenhai.political.service;

import com.chenhai.political.entity.Course_info;
import com.chenhai.political.mapper.Course_info_mapper;
import com.chenhai.political.mapper.Teacher_info_mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class Course_service {
    @Resource
    private Course_info_mapper course_info_mapper;

    public Course_info getInfo(String number) {
        return course_info_mapper.getInfo(number);
    }

    public void addCourseInfo(int id, String number, String name, String semester, int tid, String intro) {
        course_info_mapper.addCourseInfo(id, number, name, semester, tid, intro);
    }

    public void modifyCourseInfo(int id, String number, String name, String semester, String intro, int uid, int type) {
        course_info_mapper.modifyCourseInfo(id, number, name, semester, intro, uid, type);
    }

    public int getLatestID() {
        return course_info_mapper.getLatestID();
    }

    public Course_info getCourseNumber(int cid) {
        return course_info_mapper.getCourseNumber(cid);
    }

    public List<Course_info> showAllCourses() {
        return course_info_mapper.showAllCourses();
    }

    public Course_info showAssignCourse(String number) {
        return course_info_mapper.showAssignCourse(number);
    }

    public List<Course_info> getTeacherCourseInfo(int tid) {
        return course_info_mapper.getTeacherCourseInfo(tid);
    }
}
