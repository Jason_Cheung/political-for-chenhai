package com.chenhai.political.service;

import com.chenhai.political.entity.Course_detail;
import com.chenhai.political.mapper.Course_detail_mapper;
import com.chenhai.political.mapper.Teacher_info_mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Service
public class Course_detail_service {
    @Resource
    private Course_detail_mapper course_detail_mapper;

    public void addDetailInfo(int cdid, int cid, int week, Date date, String details, String events, int type, Timestamp deadline, int uid, int utype) {
        course_detail_mapper.addDetailInfo(cdid, cid, week, date, details, events, type, deadline, uid, utype);
    }

    public boolean ensureAddWeekUnique(int cid, int week) {
        // 如果返回的条数为1则返回true，代表要修改的周数发生冲突。如果为0则返回false，可以进行修改
        return course_detail_mapper.ensureAddWeekUnique(cid, week);
    }

    public void modifyDetailInfo(int cdid, int week, Date date, String details, String events, int type, Timestamp deadline, int uid, int utype) {
        course_detail_mapper.modifyDetailInfo(cdid, week, date, details, events, type, deadline, uid, utype);
    }

    public Course_detail getWeekInfo(int cid, int week) {
        return course_detail_mapper.getWeekInfo(cid, week);
    }

    public int getLatestID() {
        return course_detail_mapper.getLatestID();
    }

    public int getCourseID(int cdid) {
        return course_detail_mapper.getCourseID(cdid);
    }

    public void deleteDetailInfo(int cid, int week) {
        course_detail_mapper.deleteDetailInfo(cid, week);
    }

    public List<Course_detail> showCourseAllDetail(int cid) {
        return course_detail_mapper.showCourseAllDetail(cid);
    }

    public void modifyDetailPrincipal(int tid, int cdid) {
        course_detail_mapper.modifyDetailPrincipal(tid, cdid);
    }
}
