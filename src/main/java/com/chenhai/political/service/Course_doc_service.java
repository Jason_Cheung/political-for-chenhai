package com.chenhai.political.service;

import com.chenhai.political.entity.Course_document;
import com.chenhai.political.mapper.Course_document_mapper;
import com.chenhai.political.mapper.Teacher_info_mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class Course_doc_service {
    @Resource
    private Course_document_mapper course_document_mapper;

    public void addDocumentInfo(int docid, int detailid, String loc, int state, int uploaderid, int uploadertype) {
        course_document_mapper.addDocumentInfo(docid, detailid, loc, state, uploaderid, uploadertype);
    }

    public void deleteDocument(String loc) {
        course_document_mapper.deleteDocument(loc);
    }

    public int getLatestID() {
        return course_document_mapper.getLatestID();
    }

    public List<Course_document> showAssignCourseDocument(int detailid) {
        return course_document_mapper.showAssignCourseDocument(detailid);
    }

    public void modifyDocumentPrincipal(int tid, int docid) {
        course_document_mapper.modifyDocumentPrincipal(tid, docid);
    }

    public void addDocumentDownloadTimes(int docid) {
        course_document_mapper.addDocumentDownloadTimes(docid);
    }

    public Course_document getDocumentInfo(String loc) {
        return course_document_mapper.getDocumentInfo(loc);
    }
}

