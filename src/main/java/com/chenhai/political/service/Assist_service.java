package com.chenhai.political.service;

import com.chenhai.political.entity.Assist_info;
import com.chenhai.political.mapper.Assist_info_mapper;
import com.chenhai.political.mapper.Teacher_info_mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class Assist_service {
    @Resource
    private Assist_info_mapper assist_info_mapper;

    public Assist_info getInfo(String phone) {
        return assist_info_mapper.getInfo(phone);
    }


    public void registerAssist(int id, String phone, String name, String pwd, String img_loc) {
        assist_info_mapper.registerAssist(id, phone, name, pwd, img_loc);
    }

    public void modifyAssist(int id, String phone, String name, String pwd, String img_loc) {
        assist_info_mapper.modifyAssist(id, phone, name, pwd, img_loc);
    }

    public int getLatestID() {
        return assist_info_mapper.getLatestID();
    }

    public boolean checkLogin(String phone, String pwd) {
        return assist_info_mapper.checkLogin(phone, pwd);
    }

    public Assist_info showAssignCourseAssistInfo(int aid) {
        return assist_info_mapper.showAssignCourseAssistInfo(aid);
    }
}
